package org.elu.learn.kotlin.joy.ch07

import java.io.IOException
import java.io.Serializable

fun result() {
    println(Result(5))
    println(Result(null))
    println(Result.failure<String>("Error message"))
    println(Result.failure<String>(RuntimeException("RuntimeException")))
    println(Result.failure<String>(IOException("IOException")))
    println()

    println(Result(5).map { it / 2.0 })
    println(Result(5).map<Int> { throw IOException("IOException") })
    println(Result(5).map<Int> { throw IllegalStateException("IllegalStateException") })
    println()

    println(Result(5).flatMap { Result(it / 2.0) })
    println(Result(5).flatMap<Int> { throw IOException("IOException") })
    println(Result(5).flatMap<Int> { throw IllegalStateException("IllegalStateException") })
    println()

    val f = { 6 }
    println(Result(5).getOrElse(6))
    println(Result(5).getOrElse(f))
    println(Result.failure<Int>("Error").getOrElse(6))
    println(Result.failure<Int>("Error").getOrElse(f))
    println()

    val g = { Result(6) }
    println(Result(5).orElse(g))
    println(Result.failure<Int>("Error").orElse(g))
    val gException = { throw IOException("IOException") }
    val gRException = { throw IllegalStateException("IllegalStateException") }
    println(Result(5).orElse(gException))
    println(Result.failure<Int>("Error").orElse(gException))
    println(Result(5).orElse(gRException))
    println(Result.failure<Int>("Error").orElse(gRException))
    println()

    // Result patterns
    toonExample()
    println()

    // Advanced Result handling
    println(Result(5).filter { it % 2 == 0 })
    println(Result(6).filter { it % 2 == 0 })
    val errorMessage = "Value is odd"
    println(Result(5).filter(errorMessage) { it % 2 == 0 })
    println(Result(6).filter(errorMessage) { it % 2 == 0 })
    println(Result(5).exists { it % 2 == 0 })
    println(Result(6).exists { it % 2 == 0 })
    println()

    // Mapping failures
    val errorMessage1 = "New error message"
    println(Result.Empty.mapFailure(errorMessage1))
    println(Result(5).filter { it % 2 == 0 }.mapFailure(errorMessage1))
    println(Result(6).filter { it % 2 == 0 }.mapFailure(errorMessage1))
    println()

    // Adding factory functions
    println(Result(5, errorMessage))
    println(Result(null, errorMessage))
    println(Result(5) { it % 2 == 0 })
    println(Result(6) { it % 2 == 0 })
    println(Result(5, errorMessage) { it % 2 == 0 })
    println(Result(6, errorMessage) { it % 2 == 0 })
    println()

    // Applying effects
    var result = false
    var z = 5
    Result(if (z % 2 == 0) z else null).forEach { x -> result = (x == z) }
    println(result)
    z = 6
    Result(if (z % 2 == 0) z else null).forEach { x -> result = (x == z) }
    println(result)
    result = false
    z = 5
    Result(if (z % 2 == 0) z else null, errorMessage).forEach { x -> result = (x == z) }
    println(result)
    z = 6
    Result(if (z % 2 == 0) z else null, errorMessage).forEach { x -> result = (x == z) }
    println(result)
    println()

    var resultString: String? = ""
    z = 5
    Result(if (z % 2 == 0) z else null, errorMessage).forEachOrElse({ x -> resultString = x.toString() },
                                                                    { e -> resultString = e.message },
                                                                    {})
    println(resultString)
    resultString = ""
    z = 6
    Result(if (z % 2 == 0) z else null, errorMessage).forEachOrElse({ x -> resultString = x.toString() },
                                                                    { e -> resultString = e.message },
                                                                    {})
    println(resultString)
    resultString = ""
    z = 5
    Result(if (z % 2 == 0) z else Result<Int?>(), errorMessage).forEachOrElse({ x -> resultString = x.toString() },
                                                                              {},
                                                                              { resultString = "true" })
    println(resultString)
    resultString = ""
    z = 6
    Result(if (z % 2 == 0) z else Result<Int?>(), errorMessage).forEachOrElse({ x -> resultString = x.toString() },
                                                                              {},
                                                                              { resultString = "true" })
    println(resultString)
    println()

    resultString = ""
    z = 5
    Result(if (z % 2 == 0) z else null, errorMessage).forEach(onSuccess = { x -> resultString = x.toString() },
                                                              onFailure = { e -> resultString = e.message })
    println(resultString)
    resultString = ""
    z = 6
    Result(if (z % 2 == 0) z else null, errorMessage).forEach(onSuccess = { x -> resultString = x.toString() },
                                                              onFailure = { e -> resultString = e.message })
    println(resultString)
    resultString = ""
    z = 5
    Result(if (z % 2 == 0) z else Result<Int?>(), errorMessage).forEach(onSuccess = { x -> resultString = x.toString() },
                                                                        onEmpty = { resultString = "true" })
    println(resultString)
    resultString = ""
    z = 6
    Result(if (z % 2 == 0) z else Result<Int?>(), errorMessage).forEach(onSuccess = { x -> resultString = x.toString() },
                                                                        onEmpty = { resultString = "true" })
    println(resultString)
    println()

    // Advanced result composition
    val h: (Int) -> Int = { if (it % 5 == 0) throw RuntimeException("Should not be seen") else it }
    println(lift(h)(Result(5)))
    println(lift(h)(Result(6)))

    val h2: (Int) -> (Int) -> Int = { a ->
        { b ->
            if (a > b) throw RuntimeException("A is to big") else b - a
        }
    }
    println(lift2(h2)(Result(5))(Result(6)))
    println(lift2(h2)(Result(6))(Result(5)))

    val h3: (Int) -> (Int) -> (Int) -> Int = { a ->
        { b ->
            { c ->
                if (a > b) throw RuntimeException("A is to big") else b - a + c
            }
        }
    }
    println(lift3(h3)(Result(5))(Result(6))(Result(7)))
    println(lift3(h3)(Result(6))(Result(5))(Result(4)))
    println()

    val p: (Int) -> (Int) -> Int = { a -> { b -> if (a > b) throw RuntimeException("a is too big") else b - a } }
    println(map2(Result(5), Result(6), p))
    println(map2(Result(6), Result(5), p))
}

fun toonExample() {
    val toons: Map<String, Toon> = mapOf(
        "Mickey" to Toon("Mickey", "Mouse", "mickey@disney.com"),
        "Minnie" to Toon("Minnie", "Mouse"),
        "Donald" to Toon("Donald", "Duck", "donald@disney.com")
    )

    val toonWithFailures = getNameOld()
        .flatMap(toons::getResultOld)
        .flatMap(Toon::email)
    println(toonWithFailures)

    val toon = getName()
        .flatMap(toons::getResult)
        .flatMap(Toon::email)
    println(toon)
}

sealed class Result<out A> : Serializable {
    abstract fun <B> map(f: (A) -> B): Result<B>

    abstract fun <B> flatMap(f: (A) -> Result<B>): Result<B>

    abstract fun mapFailure(message: String): Result<A>

    abstract fun forEach(effect: (A) -> Unit)

    abstract fun forEach(onSuccess: (A) -> Unit = {},
                         onFailure: (RuntimeException) -> Unit = {},
                         onEmpty: () -> Unit = {})

    abstract fun forEachOrElse(
        onSuccess: (A) -> Unit,
        onFailure: (RuntimeException) -> Unit,
        onEmpty: () -> Unit
    )

    fun getOrElse(defaultValue: @UnsafeVariance A): A = when (this) {
        is Success -> this.value
        else -> defaultValue
    }

    fun getOrElse(defaultValue: () -> @UnsafeVariance A): A = when (this) {
        is Success -> this.value
        else -> defaultValue()
    }

    fun orElse(defaultValue: () -> Result<@UnsafeVariance A>): Result<A> = when (this) {
        is Success -> this
        else -> try {
            defaultValue()
        } catch (ex: RuntimeException) {
            failure<A>(ex)
        } catch (ex: Exception) {
            failure<A>(RuntimeException(ex))
        }
    }

    fun filter(p: (A) -> Boolean): Result<A> = flatMap {
        if (p(it)) {
            this
        } else {
            failure("Condition not matched")
        }
    }

    fun filter(message: String, p: (A) -> Boolean): Result<A> = flatMap {
        if (p(it)) {
            this
        } else {
            failure(message)
        }
    }

    fun exists(p: (A) -> Boolean): Boolean = map(p).getOrElse(false)

    internal object Empty : Result<Nothing>() {
        override fun <B> map(f: (Nothing) -> B): Result<B> = Empty
        override fun <B> flatMap(f: (Nothing) -> Result<B>): Result<B> = Empty
        override fun mapFailure(message: String): Result<Nothing> = this
        override fun forEach(effect: (Nothing) -> Unit) {}
        override fun forEach(onSuccess: (Nothing) -> Unit,
                             onFailure: (RuntimeException) -> Unit,
                             onEmpty: () -> Unit) {
            onEmpty()
        }

        override fun forEachOrElse(onSuccess: (Nothing) -> Unit,
                                   onFailure: (RuntimeException) -> Unit,
                                   onEmpty: () -> Unit) = onEmpty()

        override fun toString(): String = "Empty"
    }

    internal class Failure<out A>(internal val exception: RuntimeException) : Result<A>() {
        override fun <B> map(f: (A) -> B): Result<B> = Failure(exception)

        override fun <B> flatMap(f: (A) -> Result<B>): Result<B> = Failure(exception)

        override fun mapFailure(message: String): Result<A> =
            Failure(RuntimeException(message, exception))

        override fun forEach(effect: (A) -> Unit) {}

        override fun forEach(onSuccess: (A) -> Unit,
                             onFailure: (RuntimeException) -> Unit,
                             onEmpty: () -> Unit) {
            onFailure(exception)
        }

        override fun forEachOrElse(onSuccess: (A) -> Unit,
                                   onFailure: (RuntimeException) -> Unit,
                                   onEmpty: () -> Unit) = onFailure(exception)

        override fun toString(): String = "Failure(${exception.message})"
    }

    internal class Success<out A>(internal val value: A) : Result<A>() {
        override fun <B> map(f: (A) -> B): Result<B> = try {
            Success(f(value))
        } catch (ex: RuntimeException) {
            failure(ex)
        } catch (ex: Exception) {
            failure(RuntimeException(ex))
        }

        override fun <B> flatMap(f: (A) -> Result<B>): Result<B> = try {
            f(value)
        } catch (ex: RuntimeException) {
            failure(ex)
        } catch (ex: Exception) {
            failure(RuntimeException(ex))
        }

        override fun mapFailure(message: String): Result<A> = this

        override fun forEach(effect: (A) -> Unit) {
            effect(value)
        }

        override fun forEach(onSuccess: (A) -> Unit,
                             onFailure: (RuntimeException) -> Unit,
                             onEmpty: () -> Unit) {
            onSuccess(value)
        }

        override fun forEachOrElse(onSuccess: (A) -> Unit,
                                   onFailure: (RuntimeException) -> Unit,
                                   onEmpty: () -> Unit) = onSuccess(value)

        override fun toString(): String = "Success($value)"
    }

    companion object {
        operator fun <A> invoke(a: A? = null): Result<A> = when (a) {
            null -> Failure(NullPointerException())
            else -> Success(a)
        }

        operator fun <A> invoke(): Result<A> = Empty

        operator fun <A> invoke(a: A? = null, message: String): Result<A> = when (a) {
            null -> Failure(NullPointerException(message))
            else -> Success(a)
        }

        operator fun <A> invoke(a: A? = null, p: (A) -> Boolean): Result<A> = when (a) {
            null -> Failure(NullPointerException())
            else -> when {
                p(a) -> Success(a)
                else -> Empty
            }
        }

        operator fun <A> invoke(a: A? = null, message: String, p: (A) -> Boolean): Result<A> = when (a) {
            null -> Failure(NullPointerException())
            else -> when {
                p(a) -> Success(a)
                else -> Failure(IllegalStateException("Argument $a does not match condition: $message"))
            }
        }

        fun <A> failure(message: String): Result<A> = Failure(IllegalStateException(message))

        fun <A> failure(exception: RuntimeException): Result<A> = Failure(exception)

        fun <A> failure(exception: Exception): Result<A> = Failure(IllegalStateException(exception))
    }
}

fun <K, V> Map<K, V>.getResultOld(key: K) = when {
    this.containsKey(key) -> Result(this[key])
    else -> Result.failure("Key $key not found in map")
}

fun <K, V> Map<K, V>.getResult(key: K) = when {
    this.containsKey(key) -> Result(this[key])
    else -> Result.Empty
}

fun getNameOld(): Result<String> = try {
    validateOld(readLine())
} catch (e: IOException) {
    Result.failure(e)
}

fun getName(): Result<String> = try {
    validate(readLine())
} catch (e: IOException) {
    Result.failure(e)
}

fun validateOld(name: String?): Result<String> = when {
    name?.isNotEmpty() ?: false -> Result(name)
    else -> Result.failure("Invalid name $name")
}

fun validate(name: String?): Result<String> = when {
    name?.isNotEmpty() ?: false -> Result(name)
    else -> Result.failure(IOException())
}

data class Toon internal constructor(val firstName: String,
                                     val lastName: String,
                                     val email: Result<String>) {
    companion object {
        operator fun invoke(firstName: String,
                            lastName: String) =
            Toon(firstName, lastName, Result.Empty)

        operator fun invoke(firstName: String,
                            lastName: String,
                            email: String) =
            Toon(firstName, lastName, Result(email))
    }
}

fun <A, B> lift(f: (A) -> B): (Result<A>) -> Result<B> = { it.map(f) }
fun <A, B, C> lift2(f: (A) -> (B) -> C): (Result<A>) -> (Result<B>) -> Result<C> = { a ->
    { b ->
        a.map(f).flatMap { b.map(it) }
    }
}
fun <A, B, C, D> lift3(f: (A) -> (B) -> (C) -> D): (Result<A>) -> (Result<B>) -> (Result<C>) -> Result<D> = { a ->
    { b ->
        { c ->
            a.map(f).flatMap { b.map(it) }.flatMap { c.map(it) }
        }
    }
}

fun <A, B, C> map2(a: Result<A>,
                   b: Result<B>,
                   f: (A) -> (B) -> C): Result<C> = lift2(f)(a)(b)
