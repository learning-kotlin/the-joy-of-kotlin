package org.elu.learn.kotlin.joy.ch04

import java.math.BigInteger

fun recursiveFunctionsAndLists() {
    // Recursive functions and lists
    recursionAndList()
    println()

    // using doubly recursive functions
    fibonacciFunctions()
    println()

    // abstracting recursion on lists
    abstractingRecursion()
    println()

    // reversing a list
    reverseList()
    println()

    // building corecursive lists
    corecursiveLists()
}

val listInts = listOf(1, 2, 3, 4, 5)

// extension functions
fun <T> List<T>.head(): T =
    if (this.isEmpty()) {
        throw IllegalArgumentException("head called on empty list")
    } else {
        this[0]
    }

fun <T> List<T>.tail(): List<T> =
    if (this.isEmpty()) {
        throw IllegalArgumentException("head called on empty list")
    } else {
        this.drop(1)
    }

fun recursionAndList() {
    fun sum(list: List<Int>): Int =
        if (list.isEmpty()) 0 else list[0] + sum(list.drop(1))

    println(sum(listInts))

    fun sum1(list: List<Int>): Int =
        if (list.isEmpty()) 0 else list.head() + sum(list.tail())

    println(sum1(listInts))

    fun sum2(list: List<Int>): Int {
        tailrec fun sum(list: List<Int>, acc: Int): Int =
            if (list.isEmpty()) acc else sum(list.tail(), acc + list.head())
        return sum(list, 0)
    }
    println(sum2(listInts))
}

fun fibonacciFunctions() {
    // naive implementation
    fun fibonacciNaive(number: Int): Int =
        if (number == 0 || number == 1) {
            1
        } else {
            fibonacciNaive(number - 1) + fibonacciNaive(number - 2)
        }
    (0 until 10).forEach { print("${fibonacciNaive(it)} ") }
    println()

    fun fibonacci(n: Int): BigInteger {
        tailrec fun fib(acc1: BigInteger, acc2: BigInteger, x: BigInteger): BigInteger =
            when (x) {
                BigInteger.ZERO -> BigInteger.ONE
                BigInteger.ONE -> acc1 + acc2
                else -> fib(acc2, acc1 + acc2, x - BigInteger.ONE)
            }
        return fib(BigInteger.ZERO, BigInteger.ONE, BigInteger.valueOf(n.toLong()))
    }
    (0 until 10).forEach { print("${fibonacci(it)} ") }
    println()
    println(fibonacci(10_000))
}

fun abstractingRecursion() {
    fun <T> makeString(list: List<T>, delim: String): String =
        when {
            list.isEmpty() -> ""
            list.tail().isEmpty() ->
                "${list.head()}${makeString(list.tail(), delim)}"
            else -> "${list.head()}$delim${makeString(list.tail(), delim)}"
        }
    println(makeString(list, ":"))
    println(makeString(listInts, ":"))

    fun <T> makeString2(list: List<T>, delim: String): String {
        tailrec fun makeString(list: List<T>, acc: String): String =
            when {
                list.isEmpty() -> acc
                acc.isEmpty() -> makeString(list.tail(), "${list.head()}")
                else -> makeString(list.tail(), "$acc$delim${list.head()}")
            }
        return makeString(list, "")
    }
    println(makeString2(list, ","))
    println(makeString2(listInts, ","))

    fun <T, U> foldLeft(list: List<T>, z: U, f: (U, T) -> U): U {
        tailrec fun foldLeft(list: List<T>, acc: U): U =
            if (list.isEmpty()) {
                acc
            } else {
                foldLeft(list.tail(), f(acc, list.head()))
            }
        return foldLeft(list, z)
    }

    fun sum(list: List<Int>) = foldLeft(list, 0, Int::plus)
    fun string(list: List<Char>) = foldLeft(list, "", String::plus)
    fun <T> makeString3(list: List<T>, delim: String) =
        foldLeft(list, "") { s, t -> if (s.isEmpty()) "$t" else "$s$delim$t"}

    println(sum(listInts))
    println(string(list))
    println(makeString3(list, "|"))
    println(makeString3(listInts, "|"))

    fun string1(list: List<Char>): String =
        if (list.isEmpty()) {
            ""
        } else {
            prepend(list.head(), string(list.tail()))
        }
    println(string1(list))

    fun <T, U> foldRight(list: List<T>, identity: U, f: (T, U) -> U): U =
        if (list.isEmpty()) {
            identity
        } else {
            f(list.head(), foldRight(list.tail(), identity, f))
        }
    fun string2(list: List<Char>): String =
        foldRight(list, "") { c, s -> prepend(c, s) }
    println(string2(list))
}

fun reverseList() {
    // imperative loop
    fun <T> reverseLoop(list: List<T>): List<T> {
        val result = mutableListOf<T>()
        (list.size downTo 1).forEach {
            result.add(list[it - 1])
        }
        return result
    }
    println(reverseLoop(list))
    println(reverseLoop(listInts))

    fun <T, U> foldLeft(list: List<T>, z: U, f: (U, T) -> U): U {
        tailrec fun foldLeft(list: List<T>, acc: U): U =
            if (list.isEmpty()) {
                acc
            } else {
                foldLeft(list.tail(), f(acc, list.head()))
            }
        return foldLeft(list, z)
    }

    fun <T> prepend(list: List<T>, elem: T): List<T> = listOf(elem) + list

    fun <T> reverse(list: List<T>): List<T> =
        foldLeft(list, listOf(), ::prepend)
    println(reverse(list))
    println(reverse(listInts))

    println()
    fun <T> copy(list: List<T>): List<T> =
        foldLeft(list, listOf()) { lst, elem -> lst + elem }
    println(copy(list))
    println(copy(listInts))

    fun <T> prepend1(list: List<T>, elem: T): List<T> =
        foldLeft(list, listOf(elem)) { lst, elm -> lst + elm }
    fun <T> reverse1(list: List<T>): List<T> =
        foldLeft(list, listOf(), ::prepend1)
    // this implementation very inefficient
    println(reverse1(list))
    println(reverse1(listInts))
}

fun corecursiveLists() {
    // imperative example
    /* Java example
    for (int i = 0; i < 5; i++) {
        System.out.println(i);
    } */
    // Kotlin example
    listOf(0, 1, 2, 3, 4).forEach(::println)

    // solution using while loop
    fun rangeLoop(start: Int, end: Int): List<Int> {
        val result = mutableListOf<Int>()
        var index = start
        while (index < end) {
            result.add(index)
            index++
        }
        return result
    }
    println(rangeLoop(0, 5))

    // more abstract solution
    fun <T> unfoldLoop(seed: T, f: (T) -> T, p: (T) -> Boolean): List<T> {
        val result = mutableListOf<T>()
        var elem = seed
        while (p(elem)) {
            result.add(elem)
            elem = f(elem)
        }
        return result
    }
    fun rangeLoop2(start: Int, end: Int): List<Int> =
        unfoldLoop(start, { x -> x + 1 }, { x -> x < end })
    println(rangeLoop2(0, 5))

    fun <T> prepend(list: List<T>, elem: T): List<T> = listOf(elem) + list

    // recursive solution
    fun rangeRec(start: Int, end: Int): List<Int> =
        if (end <= start) {
            listOf()
        } else {
            prepend(rangeRec(start + 1, end), start)
        }
    println(rangeRec(0, 5))

    fun <T> unfoldRec(seed: T, f: (T) -> T, p: (T) -> Boolean): List<T> =
        if (p(seed)) {
            prepend(unfoldRec(f(seed), f, p), seed)
        } else {
            listOf()
        }
    fun rangeRec2(start: Int, end: Int): List<Int> =
        unfoldRec(start, { it + 1 }, { it < end })
    println(rangeRec2(0, 5))

    // tail recursive version
    fun <T> unfold(seed: T, f: (T) -> T, p: (T) -> Boolean): List<T> {
        tailrec fun unfold(acc: List<T>, seed: T): List<T> =
            if (p(seed)) {
                unfold(acc + seed, f(seed))
            } else {
                acc
            }
        return unfold(listOf(), seed)
    }
    fun range(start: Int, end: Int): List<Int> =
        unfold(start, { it + 1 }, { it < end })
    println(range(0, 5))
}
