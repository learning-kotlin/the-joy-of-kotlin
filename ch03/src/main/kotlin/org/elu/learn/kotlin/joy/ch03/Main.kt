package org.elu.learn.kotlin.joy.ch03

fun main() {
    // using object constructors as functions
    constructorsAsFunctions()

    val creditCard = CreditCard()
    val payment1 = Payment(creditCard, 10)
    val payment2 = Payment(creditCard, 20)
    val payment3 = Payment(creditCard, 30)

    // this is easier to read
    val newPayment1 = payment1.combine(payment2).combine(payment3)
    println(newPayment1)

    // than this
    val newPayment2 = combine(combine(payment1, payment2), payment3)
    println(newPayment2)
    println()

    // using value functions
    valueFunctions()
    println()

    // using function references
    functionReferences()
    println()

    // composing functions
    composingFunctions()
    println()

    // reusing functions
    reusingFunctions()
    println()

    // functions of several arguments
    multiAttributeFunctions()
    println()

    // higher-order functions
    higherOrderFunctions()
    println()

    // polymorphic higher-order functions
    polymorphicHofs()
    println()

    // using anonymous functions
    anonymousFunctions()
    println()

    // implementing closures
    implementClosures()
    println()

    // applying functions partially and automatic currying
    applyPartialFunc()
    println()

    // switching arguments of partially-applied functions
    switchArguments()
    println()

    // Avoiding problems with standard types
    avoidingProblems()
}

data class Person(val name: String)

fun constructorsAsFunctions() {
    val elvis = Person("Elvis")
    val theKing = Person("Elvis")
    println(elvis == theKing)
}

fun composingFunctions() {
    // following example is not a function composition
    fun square(n: Int) = n * n
    fun triple(n: Int) = n * 3
    println(square(triple(2)))

    fun compose(f: (Int) -> Int, g: (Int) -> Int): (Int) -> Int = { x -> f(g(x))}
    fun composeSimple(f: (Int) -> Int, g: (Int) -> Int): (Int) -> Int = { f(g(it))}

    val squareOfTriple = compose(::square, ::triple)
    println(squareOfTriple(2))
    println(composeSimple(::square, ::triple)(2))
}

fun reusingFunctions() {
    fun square(n: Int) = n * n
    fun triple(n: Int) = n * 3

    fun <T, U, V>compose(f: (U) -> V, g: (T) -> U): (T) -> V = { f(g(it))}
    val squareOfTriple = compose(::square, ::triple)
    println(squareOfTriple(2))
}

fun multiAttributeFunctions() {
    val add: (Int) -> (Int) -> Int = {a -> { b -> a + b }}
    println(add(1)(2))

    val mult: IntBinOp = { a -> { b -> a * b }}
    println(mult(2)(3))
}
typealias IntBinOp = (Int) -> (Int) -> Int

fun higherOrderFunctions() {
    val compose: ((Int) -> Int) -> ((Int) -> Int) -> (Int) -> Int =
        { x -> { y -> { z -> x(y(z)) } } }
    val compose1 = { x: (Int) -> Int -> { y: (Int) -> Int ->
        { z: Int -> x(y(z)) } } }
    val compose2: (IntUnaryOp) -> (IntUnaryOp) -> IntUnaryOp =
        { x -> { y -> { z -> x(y(z)) } } }

    val square: IntUnaryOp = { it * it }
    val triple: IntUnaryOp = { it * 3 }

    println(compose(square)(triple)(2))
    println(compose1(square)(triple)(2))
    println(compose2(square)(triple)(2))
}
typealias IntUnaryOp = (Int) -> Int

fun <T, U, V> higherCompose() =
    { f: (U) -> V ->
        { g: (T) -> U ->
            { x: T -> f(g(x)) }
        }
    }

fun polymorphicHofs() {
    // the following is not possible, because properties cannot be parametrised
/*  val <T, U, V> higherCompose: ((U) -> V) -> ((T) -> U) -> (T) -> V =
        { f ->
            { g ->
                { x -> f(g(x)) }
            }
        }
*/

    fun <T, U, V> higherComposeTyped(): ((U) -> V) -> ((T) -> U) -> (T) -> V =
        { f ->
            { g ->
                { x -> f(g(x)) }
            }
        }

    fun square(n: Int) = n * n
    fun triple(n: Int) = n * 3

    val squareOfTriple = higherCompose<Int, Int, Int>()(::square)(::triple)
    println(squareOfTriple(2))

    val squareOfTriple2 = higherComposeTyped<Int, Int, Int>()(::square)(::triple)
    println(squareOfTriple2(2))

    fun <T, U, V> higherAndThen(): ((T) -> U) -> ((U) -> V) -> (T) -> V =
        { f: (T) -> U ->
            { g: (U) -> V ->
                { x: T -> g(f(x)) }
            }
        }
    val other = higherAndThen<Int, Int, Int>()(::square)(::triple)
    println(other(2))
}

fun anonymousFunctions() {
    fun <T, U, V>compose(f: (U) -> V, g: (T) -> U): (T) -> V = { f(g(it))}
    val f: (Double) -> Double = { Math.PI / 2 - it }
    val sin: (Double) -> Double = Math::sin
    val cos: Double = compose(f, sin)(2.0)
    println(cos)

    // same as above using anonymous functions
    val cosValue: Double =
        compose({ x: Double -> Math.PI / 2 - x }, Math::sin)(2.0)
    println(cosValue)

    // same with higher-order function
    val cosHof =
        higherCompose<Double, Double, Double>()({ x: Double -> Math.PI / 2 - x })(Math::sin)
    println(cosHof(2.0))

    // same but lambda put outside of the parentheses
    val cosHofs =
        higherCompose<Double, Double, Double>()() { x: Double -> Math.PI / 2 - x }(Math::sin)
    println(cosHofs(2.0))
}

fun implementClosures() {
    val taxRate = 0.09

    fun addTaxCloses(price: Double) = price + price * taxRate
    println(addTaxCloses(12.0))

    fun addTaxFun(taxRate: Double, price: Double) = price + price * taxRate
    println(addTaxFun(taxRate, 12.0))

    val addTaxVal = { rate: Double, price: Double -> price + price * rate }
    println(addTaxVal(taxRate, 12.0))

    val addTaxCurried = { rate: Double ->
        { price: Double ->
            price + price * rate
        }
    }
    println(addTaxCurried(taxRate)(12.0))
}

class TaxComputer(private val rate: Double) {
    fun compute(price: Double): Double = price + price * rate
}

fun applyPartialFunc() {
    // object-oriented style
    val tc9oo = TaxComputer(0.09)
    println(tc9oo.compute(12.0))

    val addTaxCurried = { rate: Double ->
        { price: Double ->
            price + price * rate
        }
    }
    // same using curried function, partially applying
    val tc9curried = addTaxCurried(0.09)
    println(tc9curried(12.0))

    val partA = partialA(0.09, addTaxCurried)
    println(partA(12.0))

    val partB = partialB<Double, Double, Double>(12.0, addTaxCurried)
    println(partB(0.09))

    println(func(1, 2, 3, 4))
    println(funcCurried<Int, Int, Int, Int>()(1)(2)(3)(4))

    fun addTaxFun(taxRate: Double, price: Double) = price + price * taxRate
    val curried = curry(::addTaxFun)
    println(curried(0.09)(12.0))
}

fun <A, B, C> partialA(a: A, f: (A) -> (B) -> C): (B) -> C = f(a)

fun <A, B, C> partialB(b: B, f: (A) -> (B) -> C): (A) -> C =
    { a: A ->
        f(a)(b)
    }

fun <A, B, C, D> func(a: A, b: B, c: C, d: D): String = "$a $b $c $d"
fun <A, B, C, D> funcCurried(): (A) -> (B) -> (C) -> (D) -> String =
    { a: A ->
        { b: B ->
            { c: C ->
                { d: D ->
                    "$a $b $c $d"
                }
            }
        }
    }

fun <A, B, C> curry(f: (A, B) -> C): (A) -> (B) -> C =
    { a: A ->
        { b: B ->
            f(a, b)
        }
    }
