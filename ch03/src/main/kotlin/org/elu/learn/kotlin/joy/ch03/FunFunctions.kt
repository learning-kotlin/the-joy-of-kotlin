package org.elu.learn.kotlin.joy.ch03

class FunFunctions {
    var percent1 = 5
    private var percent2 = 9
    val percent3 = 13

    // pure
    fun add(a: Int, b: Int): Int = a + b

    // pure
    fun mult(a: Int, b: Int?): Int = 5

    // not pure, throws exception on division by zero
    fun div(a: Int, b: Int): Int = a / b

    // pure, on division by zero Infinity is returned
    fun div(a: Double, b: Double): Double = a / b

    // not pure, percent1 is public variable and can be changed
    fun applyTax1(a: Int): Int = a / 100 * (100 + percent1)

    // pure because no other code mutates percent2, but there is a danger
    fun applyTax2(a: Int): Int = a / 100 * (100 + percent2)

    // pure
    fun applyTax3(a: Int): Int = a / 100 * (100 + percent3)

    // not pure, it mutates it's argument
    fun append(i: Int, list: MutableList<Int>): List<Int> {
        list.add(i)
        return list
    }

    // pure
    fun append2(i: Int, list: List<Int>) = list + i
}

fun valueFunctions() {
    fun doubleFun(x: Int) = x * 2

    val doubleVal: (Int) -> Int = { x -> x * 2 }

    println(doubleFun(2))
    println(doubleVal(3))

    val doubleAndIncrement: (Int) -> Int = { x ->
        val double = x * 2
        double + 1
    }

    println(doubleAndIncrement(3))

    val add: (Int, Int) -> Int = { x, y -> x + y }
    println(add(2, 3))

    val double: (Int) -> Int = { it * 2 }
    println(double(4))
}

fun functionReferences() {
    fun double(n: Int): Int = n * 2

    val multiplyBy2Full: (Int) -> Int = { n -> double(n) }
    val multiplyBy2Short: (Int) -> Int = { double(it) }
    // Using a function reference simplifies the syntax
    val multiplyBy2: (Int) -> Int = ::double

    println(multiplyBy2Full(1))
    println(multiplyBy2Short(2))
    println(multiplyBy2(3))

    val foo = MyClass()
    val multiplyBy2Again: (Int) -> Int = foo::double

    println(multiplyBy2Again(4))

    val multiplyBy2Two: (Int) -> Int = (MyClassTwo)::double
    // above is shorthand for
    val multiplyBy2Three: (Int) -> Int = MyClassTwo.Companion::double

    println(multiplyBy2Two(5))
    println(multiplyBy2Three(6))
}

class MyClass {
    fun double(n: Int): Int = n * 2
}

class MyClassTwo {
    companion object {
        fun double(n: Int): Int = n * 2
    }
}
