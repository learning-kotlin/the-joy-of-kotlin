package org.elu.learn.kotlin.joy.ch01

data class Purchase(val donut: Donut, val payment: Payment)

data class PurchaseList(val dounuts: List<Donut>, val payment: Payment)