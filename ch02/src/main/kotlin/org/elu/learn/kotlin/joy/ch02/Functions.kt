package org.elu.learn.kotlin.joy.ch02

fun addLong(a: Int, b: Int): Int {
    return a + b
}

fun addShort(a: Int, b: Int): Int = a + b

fun addShortest(a: Int, b: Int) = a + b

fun addWrong(a: Int, b: Int) = {
    a + b
}

fun sumOfPrimes(limit: Int): Long {
    val seq: Sequence<Long> = sequenceOf(2L) +
            generateSequence(3L) {
                it + 2
            }.takeWhile {
                it < limit
            }

    fun isPrime(n: Long): Boolean =
        seq.takeWhile {
            it * it <= n
        }.all {
            n % it != 0L
        }

    return seq.filter(::isPrime).sum()
}

fun extensionFunctions() {
    val list = listOf(1, 2, 3, 4, 5, 6, 7)
    println(myLength(list))
    println(list.length())

    println(list.product())
}

fun <T> myLength(list: List<T>) = list.size

fun <T> List<T>.length() = this.size

fun List<Int>.product(): Int = this.fold(1) { a, b -> a * b }
