package org.elu.learn.kotlin.joy.ch02

import java.io.Serializable
import java.time.Instant

class PersonLong constructor(name: String) {
    val name: String

    init {
        this.name = name
    }
}

class PersonShorter constructor(name: String) {
    val name: String = name
}

class PersonMoreShorter constructor(val name: String)

class PersonShortest(val name: String)

class PersonOne(val name: String, val registered: Instant = Instant.now())

data class Person(val name: String, val registered: Instant = Instant.now()) {
    companion object {
        fun create(xml: String): Person {
            // TODO: Write an implementation creating a Person from an xml string
            return Person(xml)
        }
    }
}

open class PersonOpen(val name: String, val registered: Instant) : Serializable, Comparable<PersonOne> {
    override fun compareTo(other: PersonOne): Int {
        return 0
    }
}

class Member(name: String, registered: Instant) : PersonOpen(name, registered)

fun create(xml: String): Person {
    return Person(xml)
}
