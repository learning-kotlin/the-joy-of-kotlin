package org.elu.learn.kotlin.joy.ch05

fun singleLinkedList() {
    val list: List<Int> = List(1, 2, 3)
    println(list)
    println(list.isEmpty())
    println(List.Nil)
    println(List.Nil.isEmpty())
    println()

    // data sharing in list operations
    println(list.cons(0))
    println(list.setHead(5))
    println()

    // more list operations
    println(List.Nil.drop(1))
    println(list.drop(1))
    println(list.drop(4))
    println(List.drop(List.Nil, 4))
    println(List.drop(list, 1))
    println(List.drop(list, 4))
    // benefiting from object notation
    println(list.drop(2).setHead(0))
    println()

    println(list.dropWhile { it < 2 })
    println()

    // concatenating lists
    val list2: List<Int> = List(4, 5, 6)
    println(list.concat(list2))

    // dropping from the end of a list
    println(list)
    println(list.reverse())
    println(list.init())
    println()

    // using recursion to fold lists with higher-order functions (hofs)
    println(List.sum(list))
    println()

    println(List.product(list))
    println()

    val listDoubles: List<Double> = List(1.0, 2.0, 3.0)
    println(sum(list))
    println(product(listDoubles))

    println(foldRight(List(1, 2, 3), List()) { x: Int ->
        { y: List<Int> ->
            y.cons(x)
        }
    })

    println(list.foldRight(0) { _ -> { it + 1 } })
    println(list.length())
    println()

    // stack-safe functions
    println(List.sumSafe(list))
    println(List.productSafe(listDoubles))
    println(list.lengthSafe())
    println(list.reverseSafe())
    println(list.reverseSafeVar())
    println(list.foldRightViaFoldLeft(0) { { it + 1 } })
    println()

    // creating a stack-safe recursive version of foldRight
    println(list.cofoldRight(0) { { it + 1 } })
    println()

    println(List.concatViaFoldRight(list, list2))
    println(List.concatViaFoldLeft(list, list2))
    val mList = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
    println(mList)
    println(List.flatten(mList))
    println()

    // mapping and filtering lists
    println(triple(list))
    println(listDoubles)
    println(doubleToString(listDoubles))
    println()

    println(list)
    println(list.mapRight { it + 1 })
    println(list.mapLeft { it + 1 })
    println(list.filter { it != 2 })
    println(list.filter { it > 1 })
    println(list.flatMap { i -> List(i, -i) })
    println(list.filterFM { it < 3 })
}

sealed class List<out A> {
    abstract fun isEmpty(): Boolean

    internal object Nil: List<Nothing>() {
        override fun isEmpty() = true
        override fun toString(): String = "[NIL]"
    }

    internal class Cons<A>(internal val head: A,
                           internal val tail: List<A>): List<A>() {
        override fun isEmpty() = false
        override fun toString(): String = "[${toString("", this)}NIL]"
        private tailrec fun toString(acc: String, list: List<A>): String =
            when(list) {
                is Nil -> acc
                is Cons -> toString("$acc${list.head}, ", list.tail)
            }
    }

    fun cons(a: @UnsafeVariance A): List<A> = Cons(a, this)

    fun setHead(a: @UnsafeVariance A): List<A> =
        when(this) {
            Nil -> throw IllegalArgumentException("setHead called on an empty list")
            is Cons -> tail.cons(a)
        }

    fun drop(n: Int): List<A> = drop(this, n)
    fun dropWhile(p: (A) -> Boolean): List<A> = dropWhile(this, p)

    fun concat(list: List<@UnsafeVariance A>): List<A> = concat(this, list)

    fun init(): List<A> = reverse().drop(1).reverse()

    fun reverse(): List<A> = reverse(invoke(), this)

    fun <B> foldRight(identity: B, f: (A) -> (B) -> B): B =
        foldRight(this, identity, f)

    fun length(): Int = foldRight(0) { { it + 1 } }

    // stack-safe functions
    fun <B> foldLeft(identity: B, f: (B) -> (A) -> B): B =
        foldLeft(identity, this, f)

    fun lengthSafe(): Int = foldLeft(0) { { _ -> it + 1 } }

    fun reverseSafe(): List<A> =
        foldLeft(Nil as List<A>) { acc -> { acc.cons(it) } }

    fun reverseSafeVar(): List<A> =
        foldLeft(invoke()) { acc -> { acc.cons(it) } }

    fun <B> foldRightViaFoldLeft(identity: B, f: (A) -> (B) -> B): B =
        reverseSafe().foldLeft(identity) { x -> { y -> f(y)(x) } }

    fun <B> cofoldRight(identity: B, f: (A) -> (B) -> B): B =
        cofoldRight(identity, this.reverse(), identity, f)

    // mapping and filtering lists
    fun <B> mapLeft(f: (A) -> B): List<B> =
        foldLeft(Nil) { acc: List<B> -> { h: A -> Cons(f(h), acc) } }.reverse()

    fun <B> mapRight(f: (A) -> B): List<B> =
        cofoldRight(Nil) { h -> { t: List<B> -> Cons(f(h), t) } }

    fun filter(p: (A) -> Boolean): List<A> =
        cofoldRight(Nil) { h -> { t: List<A> -> if (p(h)) Cons(h, t) else t } }

    fun <B> flatMap(f: (A) -> List<B>): List<B> = flatten(mapLeft(f))

    fun filterFM(p: (A) -> Boolean): List<A> = flatMap { a -> if (p(a)) List(a) else Nil }

    companion object {
        operator
        fun <A> invoke(vararg az: A): List<A> =
            az.foldRight(Nil as List<A>) { a: A, list: List<A> ->
                Cons(a, list)
            }

        tailrec fun <A> drop(list: List<A>, n: Int): List<A> = when(list) {
            Nil -> list
            is Cons -> if (n <= 0) list else drop(list.tail, n - 1)
        }

        private tailrec fun <A> dropWhile(list: List<A>, p: (A) -> Boolean): List<A> = when(list) {
            Nil -> list
            is Cons -> if (p(list.head)) dropWhile(list.tail, p) else list
        }

        fun <A> concat(list1: List<A>, list2: List<A>): List<A> = when(list1) {
            Nil -> list2
            is Cons -> concat(list1.tail, list2).cons(list1.head)
        }

        tailrec fun <A> reverse(acc: List<A>, list: List<A>): List<A> =
            when (list) {
                Nil -> acc
                is Cons -> reverse(acc.cons(list.head), list.tail)
            }

        internal fun sum(ints: List<Int>): Int = when (ints) {
            Nil -> 0
            is Cons -> ints.head + sum(ints.tail)
        }

        internal fun product(ints: List<Int>): Int = when(ints) {
            Nil -> 1
            is Cons -> ints.head * product(ints.tail)
        }

        // recursive fold, may overflow the stack
        fun <A, B> foldRight(list: List<A>, identity: B, f: (A) -> (B) -> B): B =
            when (list) {
                Nil -> identity
                is Cons -> f(list.head)(foldRight(list.tail, identity, f))
            }

        // corecursive fold
        tailrec fun <A, B> foldLeft(acc: B, list: List<A>, f: (B) -> (A) -> B): B =
            when(list) {
                Nil -> acc
                is Cons -> foldLeft(f(acc)(list.head), list.tail, f)
            }

        fun sumSafe(list: List<Int>): Int = list.foldLeft(0) { x -> { y -> x + y } }

        fun productSafe(list: List<Double>): Double = list.foldLeft(1.0) { x -> { y -> x * y } }

        private tailrec fun <A, B> cofoldRight(acc: B,
                                               list: List<A>,
                                               identity: B,
                                               f: (A) -> (B) -> B): B =
            when(list) {
                Nil -> acc
                is Cons -> cofoldRight(f(list.head)(acc), list.tail, identity, f)
            }

        fun <A> concatViaFoldRight(list1: List<A>, list2: List<A>): List<A> =
            foldRight(list1, list2) { x -> { y -> Cons(x, y) } }

        fun <A> concatViaFoldLeft(list1: List<A>, list2: List<A>): List<A> =
            list1.reverse().foldLeft(list2) { x -> x::cons }

        fun <A> flatten(list: List<List<A>>): List<A> =
            list.cofoldRight(Nil) { x -> x::concat }
    }
}

fun <A, B> foldRight(list: List<A>, identity: B, f: (A) -> (B) -> B): B =
    when (list) {
        List.Nil -> identity
        is List.Cons -> f(list.head)(foldRight(list.tail, identity, f))
    }

fun sum(list: List<Int>): Int =
    foldRight(list, 0) { x -> { y -> x + y } }

fun product(list: List<Double>): Double =
    foldRight(list, 1.0) { x -> { y -> x * y } }

// mapping and filtering lists
fun triple(list: List<Int>): List<Int> =
    List.foldRight(list, List()) { x ->
        { y: List<Int> ->
            y.cons(x * 3)
        }
    }

fun doubleToString(list: List<Double>): List<String> =
    List.foldRight(list, List()) { x ->
        { y: List<String> ->
            y.cons(x.toString())
        }
    }
