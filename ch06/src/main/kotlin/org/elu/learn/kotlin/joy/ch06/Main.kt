package org.elu.learn.kotlin.joy.ch06

fun main() {
    absentValues()
    println()

    // Option use cases
    optionUseCases()
    println()

    // More samples with Option
    moreWithOption()
}
