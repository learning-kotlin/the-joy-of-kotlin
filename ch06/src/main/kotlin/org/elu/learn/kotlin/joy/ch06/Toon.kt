package org.elu.learn.kotlin.joy.ch06

import kotlin.math.pow

data class Toon(
    val firstName: String,
    val lastName: String,
    val email: Option<String> = Option()
) {

    companion object {
        operator fun invoke(
            firstName: String,
            lastName: String,
            email: String? = null
        ) =
            Toon(firstName, lastName, Option(email))
    }
}

fun <K, V> Map<K, V>.getOption(key: K) = Option(this[key])

fun optionUseCases() {
    val toons: Map<String, Toon> = mapOf(
        "Mickey" to Toon("Mickey", "Mouse", "mickey@disney.com"),
        "Minnie" to Toon("Minnie", "Mouse"),
        "Donald" to Toon("Donald", "Duck", "donald@disney.com"))

    val mickey = toons.getOption("Mickey").flatMap { it.email }
    val minnie = toons.getOption("Minnie").flatMap { it.email }
    val goofy = toons.getOption("Goofy").flatMap { it.email }

    println(mickey.getOrElse { "No data" })
    println(minnie.getOrElse { "No data" })
    println(goofy.getOrElse { "No data" })

    // more idiomatic Kotlin solution
    val mickeyEmail = toons["Mickey"]?.email ?: "No data"
    val minnieEmail = toons["Minnie"]?.email ?: "No data"
    val goofyEmail = toons["Goofy"]?.email ?: "No data"

    println(mickeyEmail)
    println(minnieEmail)
    println(goofyEmail)
    println()

    val listDoubles = listOf(3.0, 5.0, 7.0, 2.0, 1.0)
    println(variance(listDoubles))
    println(variance(listOf()))
    println(varianceFun(listDoubles))
    println(varianceFun(listOf()))
    println()

    println(aToBFunction(1))
    println(aToBFunctionVal(1))
    println(aToBFunctionValByRef(1))
    println(aToBFunctionFunc(1))
}

val mean: (kotlin.collections.List<Double>) -> Option<Double> = { list ->
    when {
        list.isEmpty() -> Option()
        else -> Option(list.sum() / list.size)
    }
}
val variance: (kotlin.collections.List<Double>) -> Option<Double> = { list ->
    mean(list).flatMap { m ->
        mean(list.map { x ->
            (x - m).pow(2.0)
        })
    }
}

fun meanFun(list: kotlin.collections.List<Double>): Option<Double> =
    when {
        list.isEmpty() -> Option()
        else -> Option(list.sum() / list.size)
    }

fun varianceFun(list: kotlin.collections.List<Double>): Option<Double> =
    meanFun(list).flatMap { m ->
        meanFun(list.map { x ->
            (x - m).pow(2.0)
        })
    }

fun aToBFunction(a: Int): Double {
    return a * 1.0
}
val aToBFunctionVal: (Int) -> Double = { a -> aToBFunction(a) }
val aToBFunctionValByRef: (Int) -> Double = ::aToBFunction
fun aToBFunctionFunc(a: Int): Double = aToBFunctionVal(a)
