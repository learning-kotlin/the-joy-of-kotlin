package org.elu.learn.kotlin.joy.ch07

fun errorHandling() {
    println(Either.left<String, String>("one"))
    println(Either.right<String, String>("two"))
    println()

    val list = List(3, 5, 7, 2, 1)
    val maxOk = max(list)
    val maxNok = max(List<Int>())
    println(maxOk)
    println(maxNok)
    println()

    println(maxOk.map { it / 2.0 })
    println(maxNok.map { it / 2.0 })
    println(maxOk.flatMap { Either.right<String, Double>(it / 2.0) })
    println(maxNok.map { Either.right<String, Double>(it / 2.0) })
    println(maxNok.map { Either.left<String, Double>("Error") })
    println()

    println(maxOk.getOrElse { -100 })
    println(maxNok.getOrElse { -100 })
    println(maxOk.orElse { Either.right(-100) })
    println(maxNok.orElse { Either.right(-100) })
    println(maxNok.orElse { Either.left("Error 1") })
}

sealed class Either<E, out A> {
    abstract fun <B> map(f: (A) -> B): Either<E, B>
    abstract fun <B> flatMap(f: (A) -> Either<E, B>): Either<E, B>

    fun getOrElse(defaultValue: () -> @UnsafeVariance A): A = when (this) {
        is Right -> this.value
        is Left  -> defaultValue()
    }

    fun orElse(defaultValue: () -> Either<E, @UnsafeVariance A>): Either<E, A> =
        map { this }.getOrElse(defaultValue)

    internal class Left<E, out A>(private val value: E): Either<E, A>() {
        override fun toString() = "Left($value)"
        override fun <B> map(f: (A) -> B): Either<E, B> = Left(value)
        override fun <B> flatMap(f: (A) -> Either<E, B>): Either<E, B> = Left(value)
    }

    internal class Right<E, out A>(internal val value: A): Either<E, A>() {
        override fun toString() = "Right($value)"
        override fun <B> map(f: (A) -> B): Either<E, B> = Right(f(value))
        override fun <B> flatMap(f: (A) -> Either<E, B>): Either<E, B> = f(value)
    }

    companion object {
        fun <A, B> left(value: A): Either<A, B> = Left(value)
        fun <A, B> right(value: B): Either<A, B> = Right(value)
    }
}

fun <A: Comparable<A>> max(list: List<A>): Either<String, A> = when (list) {
    is List.Nil -> Either.left("max called on an empty list")
    is List.Cons -> Either.right(list.foldLeft(list.head) { x ->
        { y -> if (x.compareTo(y) == 1) x else y }
    })
}
