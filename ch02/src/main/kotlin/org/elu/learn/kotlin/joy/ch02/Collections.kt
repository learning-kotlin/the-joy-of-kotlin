package org.elu.learn.kotlin.joy.ch02

fun collectionSample() {
    val list1 = listOf(1, 2, 3)
    val list2 = list1 + 4
    val list3 = list1 + list2
    println(list1)
    println(list2)
    println(list3)
    println()

    val mlist1 = mutableListOf(1, 2, 3)
    val mlist2 = mlist1.add(4)
    val mlist3 = mlist1.addAll(list2)
    println(mlist1)
    println(mlist2)
    println(mlist3)
}
