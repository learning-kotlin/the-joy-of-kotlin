package org.elu.learn.kotlin.joy.ch02

import java.io.File
import java.time.ZoneId
import kotlin.random.Random

fun conditionalSamples() {
    val a: Int = Random.nextInt()
    val b: Int = Random.nextInt()

    val s = if (a < b) {
        "a is smaller than b"
    } else {
        "a is not smaller than b"
    }

    println(s)

    val percent = if (b != 0) {
        val temp = a / b
        temp * 100
    } else {
        0
    }

    println(percent)

    val country = "Finland"

    val capital = when (country) {
        "Australia" -> "Canberra"
        "Bolivia"   -> "Sucre"
        "Brazil"    -> "Brasilia"
        "Finland"   -> "Helsinki"
        else        -> "Unknown"
    }
    println(capital)

    val tired = Random.nextBoolean()

    val cap = when {
        tired                  -> "Check for yourself"
        country == "Australia" -> "Canberra"
        country == "Bolivia"   -> "Sucre"
        country == "Brazil"    -> "Brasília"
        country == "Finland"   -> "Helsinki"
        else                   -> "Unknown"
    }
    println(cap)
}

fun loopSamples() {
    for (i in 0 until 10 step 2) {
        print("$i ")
    }
    println()

    // this is equivalent to
    val range = 0 until 10 step 2
    for (i in range) print("$i ")
    println()

    for (i in 1..10) print("$i ")
    println()

    for (i in 10 downTo 1) print("$i ")
    println()
}

fun exceptionSamples() {
    val arg: String = if (Random.nextBoolean()) "100" else "abc"
    val num: Int = try {
        arg.toInt()
    } catch (e: Exception) {
        0
    } finally {
        // Code in this block is always executed
    }
    println(num)
}

fun autoResourceClosure() {
    // example with side effect
    File("gradle.properties").inputStream()
        .use {
            it.bufferedReader()
                .lineSequence()
                .forEach(::println)
        }

    // next example throws IOException: Stream Closed
    /*val lines: Sequence<String> = File("gradle.properties").inputStream()
        .use {
            it.bufferedReader()
                .lineSequence()
        }
    lines.forEach(::println)*/

    // solution is to force stream evaluation before exiting the block
    val lines: List<String> = File("gradle.properties").inputStream()
        .use {
            it.bufferedReader()
                .lineSequence()
                .toList()
        }
    lines.forEach(::println)

    // Kotlin offers a much simpler way to do this
    File("gradle.properties").forEachLine { println(it) }

    // can also use useLines, which returns a Sequence
    File("gradle.properties").useLines { it.forEach(::println) }
}

fun smartCast() {
    val payload: Any = if (Random.nextBoolean()) "some payload" else if (Random.nextBoolean()) 5 else 6L

    val length = if (payload is String) payload.length else -1

    println(length)

    val result: Int = when (payload) {
        is String   -> payload.length
        is Int      -> payload
        else        -> -1
    }
    println(result)

    val result2: String? = payload as? String
    println(result2)
}

fun stringSamples() {
    // string interpolation
    val (name, date) = Person("Edu")
    println("$name's registration date: $date")

    println("$name's registration date: ${date.atZone(ZoneId.of("Europe/Helsinki"))}")

    // multi-line strings
    println("""This is the first line
          |and this is the second one.""".trimMargin()) // "|" is a margin limit
}
