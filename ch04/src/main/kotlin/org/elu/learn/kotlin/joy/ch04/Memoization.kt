package org.elu.learn.kotlin.joy.ch04

import java.lang.StringBuilder
import java.math.BigInteger
import java.util.concurrent.ConcurrentHashMap

fun memoize() {
    // solution using loop
    fun fiboLoop(limit: Int): String =
        when {
            limit < 1 -> throw IllegalArgumentException()
            limit == 1 -> "1"
            else -> {
                var fibo1 = BigInteger.ONE
                var fibo2 = BigInteger.ONE
                var fibonacci: BigInteger
                val builder = StringBuilder("1, 1")
                for (i in 2 until limit) {
                    fibonacci = fibo1.add(fibo2)
                    builder.append(", ").append(fibonacci)
                    fibo1 = fibo2
                    fibo2 = fibonacci
                }
                builder.toString()
            }
        }
    println(fiboLoop(10))

    // recursive solution
    fun fibo(limit: Int): String {
        tailrec fun fibo(acc: List<BigInteger>, acc1: BigInteger, acc2: BigInteger, x: BigInteger): List<BigInteger> =
            when (x) {
                BigInteger.ZERO -> acc
                BigInteger.ONE -> acc + (acc1 + acc2)
                else -> fibo(acc + (acc1 + acc2), acc2, acc1 + acc2, x - BigInteger.ONE)
            }
        val list = fibo(listOf(), BigInteger.ONE, BigInteger.ZERO, BigInteger.valueOf(limit.toLong()))
        return list.joinToString(", ")
    }
    println(fibo(10))
    println()

    val f = { x: Pair<BigInteger, BigInteger> -> Pair(x.second, x.first + x.second) }
    println(f(BigInteger.ONE to BigInteger.ONE))

    val f1 = { (a, b): Pair<BigInteger, BigInteger> -> Pair(b, a + b) }
    println(f1(BigInteger.ONE to BigInteger.ONE))
    println()

    // corecursive solution
    fun <T> iterate(seed: T, f: (T) -> T, n: Int): List<T> {
        tailrec fun iterate(acc: List<T>, seed: T): List<T> =
            if (acc.size < n) {
                iterate(acc + seed, f(seed))
            } else {
                acc
            }
        return iterate(listOf(), seed)
    }
    fun <T, U> map(list: List<T>, f: (T) -> U): List<U> {
        tailrec fun map(acc: List<U>, list: List<T>): List<U> =
            if (list.isEmpty()) {
                acc
            } else {
                map(acc + f(list.head()), list.tail())
            }
        return map(listOf(), list)
    }
    fun fiboCorecursive(limit: Int): String {
        val seed = Pair(BigInteger.ZERO, BigInteger.ONE)
        val func = { x: Pair<BigInteger, BigInteger> -> Pair(x.second, x.first + x.second )}
        val listOfPairs = iterate(seed, func, limit + 1)
        val list = map(listOfPairs) { p -> p.first }
        return list.joinToString(", ")
    }
    println(fiboCorecursive(10))
    println()

    // memoization using traditional approach
    val cache = mutableMapOf<Int, Int>()
    fun double(x: Int) =
        if (cache.containsKey(x)) {
            cache[x]
        } else {
            val result = x * 2
            cache[x] = result
            result
        }
    println(double(1))
    println(double(2))
    println(double(2))
    println(double(3))

    // using language feature for same
    fun doubleFun(x: Int) = cache.computeIfAbsent(x) { it * 2 }
    val doubleVal: (Int) -> Int = { cache.computeIfAbsent(it) { it * 2 }}
    println(doubleFun(2))
    println(doubleVal(2))
    println()

    // encapsulating cache
    println(Doubler.double(2))
    println(Doubler.double(2))
    println(Doubler.double(3))
    println()

    // memoizing functions without rewriting it
    fun longComputation(number: Int): Int {
        Thread.sleep(1000)
        return number
    }

    val startTime1 = System.currentTimeMillis()
    val result1 = longComputation(42)
    val time1 = System.currentTimeMillis() - startTime1
    val memoizedLongComputation = Memoizer.memoize(::longComputation)
    val startTime2 = System.currentTimeMillis()
    val result2 = memoizedLongComputation(42)
    val time2 = System.currentTimeMillis() - startTime2
    val startTime3 = System.currentTimeMillis()
    val result3 = memoizedLongComputation(42)
    val time3 = System.currentTimeMillis() - startTime3
    println("Call to nonmemoized function: result = $result1, time = $time1")
    println("First call to memoized function: result = $result2, time = $time2")
    println("Second call to memoized function: result = $result3, time = $time3")
    println()

    // implementing memoization of multi-argument functions
    val mhc = Memoizer.memoize { x: Int ->
        Memoizer.memoize { y: Int ->
            x + y
        }
    }
    println(mhc(1)(2))
    println(mhc(1)(2))

    val f3m = Memoizer.memoize { x: Int ->
        Memoizer.memoize { y: Int ->
            Memoizer.memoize { z: Int ->
                longComputation(z) - (longComputation(y) + longComputation(x))
            }
        }
    }
    val startTime11 = System.currentTimeMillis()
    val result11 = f3m(41)(42)(43)
    val time11 = System.currentTimeMillis() - startTime11
    val startTime12 = System.currentTimeMillis()
    val result12 = f3m(41)(42)(43)
    val time12 = System.currentTimeMillis() - startTime12
    println("First call to memoized function: result = $result11, time = $time11")
    println("Second call to memoized function: result = $result12, time = $time12")
    println()

    val ft = { (a, b, c, d): Tuple4<Int, Int, Int, Int> ->
        longComputation(a) + longComputation(b) - longComputation(c) * longComputation(d) }

    val ftm = Memoizer.memoize(ft)

    val startTime21 = System.currentTimeMillis()
    val result21 = ftm(Tuple4(40, 41, 42, 43))
    val time21 = System.currentTimeMillis() - startTime21
    val startTime22 = System.currentTimeMillis()
    val result22 = ftm(Tuple4(40, 41, 42, 43))
    val time22 = System.currentTimeMillis() - startTime22
    println("First call to memoized function: result = $result21, time = $time21")
    println("Second call to memoized function: result = $result22, time = $time22")
}

// encapsulating cache
object Doubler {
    private val cache = mutableMapOf<Int, Int>()
    fun double(x: Int) = cache.computeIfAbsent(x) { it * 2 }
}

class Memoizer<T, U> private constructor() {
    private val cache = ConcurrentHashMap<T, U>()
    private fun doMemoize(function: (T) -> U): (T) -> U =
        { input ->
            cache.computeIfAbsent(input) {
                function(it)
            }
        }

    companion object {
        fun <T, U> memoize(function: (T) -> U): (T) -> U =
            Memoizer<T, U>().doMemoize(function)
    }
}

data class Tuple4<T, U, V, W>(val first: T,
                              val second: U,
                              val third: V,
                              val fourth: W)
