package org.elu.learn.kotlin.joy.ch02

fun lambdaSamples() {
    val list = listOf(1, 2, 3, 4, 5, 6, 7)
    println(list)
    println(triple(list))
    println(tripleReadable(list))
    println(product(list))
    println(productMultiline(list))
    println(tripleSimplified(list))

    val multiplier = 3
    fun multiplyAll(list: List<Int>): List<Int> = list.map { it * multiplier }
    println(multiplyAll(list))
}

fun triple(list: List<Int>): List<Int> = list.map({ a -> a * 3 })

fun tripleReadable(list: List<Int>): List<Int> = list.map { a -> a * 3 }

fun product(list: List<Int>): Int = list.fold(1) { a, b -> a * b }

fun productMultiline(list: List<Int>): Int = list.fold(1) { a, b ->
    val result = a * b
    result
}

fun tripleSimplified(list: List<Int>): List<Int> = list.map { it * 3 }

