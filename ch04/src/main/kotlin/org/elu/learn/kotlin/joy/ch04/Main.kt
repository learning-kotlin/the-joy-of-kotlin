package org.elu.learn.kotlin.joy.ch04

fun main() {
    // Corecursion and recursion
    runRecursion()
    println()

    recursiveFunctionsAndLists()
    println()

    // Memoization
    memoize()
}
