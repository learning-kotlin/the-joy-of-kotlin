package org.elu.learn.kotlin.joy.ch03

fun switchArguments() {
    val addTax: (Double) -> (Double) -> Double =
        { x ->
            { y ->
                y + y / 100 * x
            }
        }

    val add9percentTax: (Double) -> Double = addTax(9.0)
    println(add9percentTax(12.0))

    val addTaxReversed: (Double) -> (Double) -> Double =
        { x ->
            { y ->
                x + x / 100 * y
            }
        }
    val addTaxBack = swapArgs(addTaxReversed)
    val tc9 = addTaxBack(9.0)
    println(tc9(12.0))
}

fun <T, U, V> swapArgs(f: (T) -> (U) -> V): (U) -> (T) -> (V) =
    { u: U -> { t: T -> f(t)(u) } }


fun avoidingProblems() {
    val toothPaste = Product("Tooth paste", 1.5, 0.5)
    val toothBrush = Product("Tooth brush", 3.5, 0.3)
    val orderLines = listOf(
        OrderLine(toothPaste, 2),
        OrderLine(toothBrush, 3))
    // incorrectly mixed prices and weights, which the compiler could not notice
    val weight = orderLines.sumByDouble { it.amount() }
    val price = orderLines.sumByDouble { it.weight() }
    println("Total price: $price")
    println("Total weight: $weight")

    // solving problem above
    val totalPrice = Price(1.0) + Price(2.0)
    println(totalPrice)
    println(Weight(2.0) + Weight(3.0))
    println(totalPrice.times(2))

    val zeroPrice = Price(0.0)
    val zeroWeight = Weight(0.0)
    val toothPasteTyped = ProductTyped("Tooth paste", Price(1.5), Weight(0.5))
    val toothBrushTyped = ProductTyped("Tooth brush", Price(3.5), Weight(0.3))
    val orderLinesTyped = listOf(
        OrderLineTyped(toothPasteTyped, 2),
        OrderLineTyped(toothBrushTyped, 3))

    val weightFixed: Weight = orderLinesTyped.fold(zeroWeight) { a, b -> a + b.weight() }
    val priceFixed: Price = orderLinesTyped.fold(zeroPrice) { a, b -> a + b.amount() }
    println("Total price: $priceFixed")
    println("Total weight: $weightFixed")

    // improving it further
    val toothPasteA = ProductImproved("Tooth paste", PriceImproved(1.5), WeightImproved(0.5))
    val toothBrushA = ProductImproved("Tooth brush", PriceImproved(3.5), WeightImproved(0.3))
    val orderLinesA = listOf(
        OrderLineImproved(toothPasteA, 2),
        OrderLineImproved(toothBrushA, 3)
    )
    val weightA: WeightImproved =
        orderLinesA.fold(WeightImproved.identity) { a, b ->
            a + b.weight()
        }
    val priceA: PriceImproved =
        orderLinesA.fold(PriceImproved.identity) { a, b ->
            a + b.amount()
        }
    println("Total price: $priceA")
    println("Total weight: $weightA")
}

data class Product(val name: String, val price: Double, val weight: Double)

data class OrderLine(val product: Product, val count: Int) {
    fun weight() = product.weight * count
    fun amount() = product.price * count
}

data class Price(val value: Double) {
    operator fun plus(price: Price) = Price(this.value + price.value)
    operator fun times(num: Int) = Price(this.value * num)
}
data class Weight(val value: Double) {
    operator fun plus(weight: Weight) = Weight(this.value + weight.value)
    operator fun times(num: Int) = Weight(this.value * num)
}

data class ProductTyped(val name: String, val price: Price, val weight: Weight)

data class OrderLineTyped(val product: ProductTyped, val count: Int) {
    fun weight() = product.weight * count
    fun amount() = product.price * count
}

data class PriceImproved private constructor(private val value: Double) {
    override fun toString() = value.toString()
    operator fun plus(price: PriceImproved) = PriceImproved(this.value + price.value)
    operator fun times(num: Int) = PriceImproved(this.value * num)

    companion object {
        val identity = PriceImproved(0.0)

        operator fun invoke(value: Double) =
            if (value > 0) {
                PriceImproved(value)
            } else {
                throw IllegalArgumentException("Price must be positive or null")
            }
    }
}
data class WeightImproved(val value: Double) {
    override fun toString() = value.toString()
    operator fun plus(weight: WeightImproved) = WeightImproved(this.value + weight.value)
    operator fun times(num: Int) = WeightImproved(this.value * num)

    companion object {
        val identity = WeightImproved(0.0)

        operator fun invoke(value: Double) =
            if (value > 0) {
                PriceImproved(value)
            } else {
                throw IllegalArgumentException("Weight must be positive or null")
            }
    }
}
data class ProductImproved(val name: String, val price: PriceImproved, val weight: WeightImproved)
data class OrderLineImproved(val product: ProductImproved, val count: Int) {
    fun weight() = product.weight * count
    fun amount() = product.price * count
}
