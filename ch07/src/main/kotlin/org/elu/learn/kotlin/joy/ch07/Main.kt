package org.elu.learn.kotlin.joy.ch07

fun main() {
    // The Either type
    errorHandling()
    println()

    // The Result type
    result()
}
