package org.elu.learn.kotlin.joy.ch06

fun absentValues() {
    // old style of dealing with optional data, null based
    val list = listOf(3, 5, 7, 2, 1)
    println(mean(listOf()))
    println(mean(list))

    println("The maximum is ${maxOld(listOf(), 0)}")
    println("The maximum is ${maxOld(list, 0)}")

    val maxUnsafe = listOf(1, 2, 3).max()
    if (maxUnsafe != null) println("The maximum is $maxUnsafe")
    println()

    // Using the Option type
    println(max(listOf()))
    val max = max(list)
    if (!max.isEmpty()) println("Maximum is $max")
    println()

    // getting a value from an Option
    val max1 = max(list).getOrElse(0)
    println("Maximum is $max1")
    val max2 = max(listOf()).getOrElse(0)
    println("Maximum is $max2")

    // next line will throw exception
    // max(list).getOrElse(getDefault())
    println(max(list).getOrElse(::getDefault))
    try {
        println(max(listOf()).getOrElse(::getDefault))
    } catch (ex: Exception) {
        println("Exception caught")
    }
    println()

    // applying functions to optional values
    println(max.map { it + 1 })
    println()

    // dealing with Option composition
    val none = max(listOf())
    println(max.flatMap { Option(it / 2) })
    println(none.flatMap { Option(it / 2) })

    val default = Option(42)
    println(max.orElse { default })
    println(none.orElse { default })

    val p: (Int) -> Boolean = { it % 5 != 0 }
    println(max.filter(p))
    println(none.filter(p))
}

fun mean(list: kotlin.collections.List<Int>): Double = when {
    list.isEmpty() -> Double.NaN
    else -> list.sum().toDouble() / list.size
}

fun maxOld(list: kotlin.collections.List<Int>, default: Int) = list.max() ?: default

sealed class Option<out A> {
    abstract fun isEmpty(): Boolean

    fun <B> map(f: (A) -> B): Option<B> = when (this) {
        is None -> None
        is Some -> Some(f(value))
    }

    fun getOrElse(default: @UnsafeVariance A): A = when (this) {
        is None -> default
        is Some -> value
    }

    fun getOrElse(default: () -> @UnsafeVariance A): A = when (this) {
        is None -> default()
        is Some -> value
    }

    fun <B> flatMap(f: (A) -> Option<B>): Option<B> = map(f).getOrElse(None)

    fun orElse(default: () -> Option<@UnsafeVariance A>): Option<A> =
        map { this }.getOrElse(default)

    fun filter(p: (A) -> Boolean): Option<A> =
        flatMap { x -> if (p(x)) this else None }

    internal object None : Option<Nothing>() {
        override fun isEmpty() = true
        override fun toString() = "None"
        override fun equals(other: Any?): Boolean = other === None
        override fun hashCode(): Int = 0
    }

    internal data class Some<out A>(internal val value: A) : Option<A>() {
        override fun isEmpty() = false
    }

    companion object {
        operator fun <A> invoke(a: A? = null): Option<A> =
            when (a) {
                null -> None
                else -> Some(a)
            }
    }
}

fun max(list: kotlin.collections.List<Int>): Option<Int> = Option(list.max())

fun getDefault(): Int = throw RuntimeException()
