package org.elu.learn.kotlin.joy.ch02

import java.time.Instant

fun main() {
    val name: String by lazy { getName() }
    println("hey1")
    val name2: String by lazy { name }
    println("hey2")

    println(name)
    println(name2)
    println(name)
    println(name2)
    println()

    lateinit var name3: String
    println("hey3")
    name3 = getName()
    println("hey4")
    println(name3)

    println()
    PersonLong(name)
    PersonShorter(name2)
    PersonMoreShorter(name3)
    PersonShortest(name3)

    val member = Member("Bob", Instant.now())
    println(member)

    val person = PersonOne("Bob")
    println("${person.name} ${person.registered}")

    val aku = Person(name)
    println(aku)
    println()

    show(listOf(Person("John"), Person("Paul")))
    showToo(listOf(Person("John"), Person("Paul")))
    println()

    Person.create("some xml string")

    println(org.elu.learn.kotlin.joy.ch02.create("another xml"))
    // import org.elu.learn.kotlin.joy.ch02.*
    println(create("yet another xml"))
    println()

    // Collections
    collectionSample()
    println()

    // Functions
    println(addLong(1, 2))
    println(addShort(2, 3))
    println(addShortest(3, 4))
    println(addWrong(4, 5))
    println(addWrong(4, 5)())

    println()
    println(sumOfPrimes(100))

    println()
    extensionFunctions()
    println()

    // Lambdas
    lambdaSamples()
    println()

    // Null Safety
    nullSafetyExample()
    println()

    // Flow control
    // Conditionals
    conditionalSamples()
    println()

    // Loops
    loopSamples()
    println()

    // Exception
    exceptionSamples()
    println()

    // Automatic resource closure
    autoResourceClosure()
    println()

    // Kotlin's smart cast
    smartCast()
    println()

    // String manipulation
    stringSamples()
    println()

    // Variance
    varianceSample()
}

fun show(persons: List<Person>) {
    for ((name, date) in persons) {
        println("$name's registration date: $date")
    }
}

fun showToo(persons: List<Person>) {
    for (person in persons) {
        println("${person.name}'s registration date: ${person.registered}")
    }
}

fun getName(): String {
    println("computing name...")
    return "Aku Ankka"
}
