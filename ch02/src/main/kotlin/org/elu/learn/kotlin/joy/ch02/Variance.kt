package org.elu.learn.kotlin.joy.ch02

fun varianceSample() {
    // why is variance a potential problem?
    val s = "A String"
    val a: Any = s
    println(a)

    val ls = mutableListOf("A string")
    // next line won't compile
    //val la: MutableList<Any> = ls
    // but if it would, then it would be possible to insert Int into list of Strings
    //la.add(42)

    val la = ls + 42
    println(ls)
    println(la)

    val laa: MutableList<Any> = mutableListOf()
    // next line won't compile
    // addAll(laa, ls)
    addAll1(laa, ls)
    println(laa)
    println()

    // covariant is used when the type is only output (return values)
    val bagOut: BagOut<MyClassParent> = BagOutImpl()
    println(bagOut.get())

    // contravariant is used when the type is only input (arguments)
    val bagIn: BagIn<MyClass> = BagInImpl()
    println(bagIn.use(MyClass()))

    // next line will not compile
    // val bag = useBagFailed(BagImpl())
    val bag1 = useBag(BagImpl())
    println(bag1)

    val bag2 = createBag()
    println(bag2.get())
}

fun <T> addAll(list1: MutableList<T>, list2: MutableList<T>) {
    for (elem in list2) list1.add(elem)
}

// out keyword is used to indicate that list2 is a covariant on T
fun <T> addAll1(list1: MutableList<T>, list2: MutableList<out T>) {
    for (elem in list2) list1.add(elem)
}

open class MyClassParent

class MyClass: MyClassParent()

interface BagOut<out T> {
    fun get(): T
}

class BagOutImpl : BagOut<MyClass> {
    override fun get(): MyClass = MyClass()
}

interface BagIn<in T> {
    fun use(t: T): Boolean
}

class BagInImpl : BagIn<MyClassParent> {
    override fun use(t: MyClassParent) = true
}

// when interface both consumes and produces a type T, it can’t be variant
interface Bag<T> {
    fun get(): T
    fun use(t: T): Boolean
}

class BagImpl : Bag<MyClassParent> {
    override fun get(): MyClassParent =  MyClassParent()

    override fun use(t: MyClassParent) = true
}

fun useBagFailed(bag: Bag<MyClass>): Boolean {
    // do something with bag
    return true
}

fun useBag(bag: Bag<in MyClass>): Boolean {
    // do something with bag
    return true
}

class BagImpl2 : Bag<MyClass> {
    override fun use(t: MyClass): Boolean = true
    override fun get(): MyClass = MyClass()
}
fun createBag(): Bag<out MyClassParent> = BagImpl2()
