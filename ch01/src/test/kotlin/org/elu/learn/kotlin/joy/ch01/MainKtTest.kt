package org.elu.learn.kotlin.joy.ch01

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

class MainKtTest {
    @Test
    fun `test buy donuts`() {
        val creditCard = CreditCard()
        val purchaseList = buyDonuts(5, creditCard)
        assertNotNull(purchaseList)
        assertEquals(Donut.price * 5, purchaseList.payment.amount)
        assertEquals(creditCard, purchaseList.payment.creditCard)
    }
}
