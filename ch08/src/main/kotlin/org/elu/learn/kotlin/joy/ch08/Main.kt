package org.elu.learn.kotlin.joy.ch08

fun main() {
    val list = List(3, 5, 7, 2, 1)
    println(list.lengthMemoized())
    println(list.length)
    println(List<Int>().lengthMemoized())
    println(List<Int>().length)
}
