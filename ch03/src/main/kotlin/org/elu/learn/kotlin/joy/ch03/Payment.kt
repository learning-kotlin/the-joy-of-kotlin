package org.elu.learn.kotlin.joy.ch03

data class Payment(val creditCard: CreditCard, val amount: Int) {
    fun combine(payment: Payment): Payment =
        if (creditCard == payment.creditCard)
            Payment(creditCard, amount + payment.amount)
        else
            throw IllegalStateException("Cards don't match.")

    companion object {
        fun groupByCard(payments: List<Payment>): List<Payment> =
            payments.groupBy { it.creditCard }
                .values
                .map { it.reduce(Payment::combine) }
    }
}

fun combine(payment1: Payment, payment2: Payment): Payment =
    if (payment1.creditCard == payment2.creditCard)
        Payment(payment1.creditCard, payment1.amount + payment2.amount)
    else
        throw IllegalStateException("Cards don't match.")
