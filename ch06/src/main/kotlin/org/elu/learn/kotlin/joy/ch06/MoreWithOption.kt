package org.elu.learn.kotlin.joy.ch06

fun moreWithOption() {
    // other ways to combine Options
    val upperOption: (Option<String>) -> Option<String> =
        lift { it.toUpperCase() }

    val upperOptionRef: (Option<String>) -> Option<String> = lift(String::toUpperCase)

    println(upperOption(Option("abc")))
    println(upperOptionRef(Option("abc")))

    println(cLift<String, String> { it.toUpperCase() }(Option("abc")))
    println(cLift<String, String> { throw RuntimeException() }(Option("abc")))
    println(hLift<String, String> { it.toUpperCase() }("abc"))
    println(hLift<String, String> { throw RuntimeException() }("abc"))
    println()

    val parseWithRadix: (Int) -> (String) -> Int =
        { radix -> { string -> Integer.parseInt(string, radix) } }

    val parseHex: (String) -> Int = parseWithRadix(16)
    println(parseHex("128"))
    println()

    val codePoint: (CharArray) -> (Int) -> (Int) -> Int =
        { array -> { index -> { limit -> Character.codePointAt(array, index, limit) } } }

    println(map2(Option(16), Option("128"), parseWithRadix))
    println(map3(Option(charArrayOf('a', 'b', 'c', 'd')), Option(1), Option.invoke(2), codePoint))
    println()

    // composing list with Option
    val list = List(3, 5, 7, 2, 1)
    val result = sequence(list.map { Option(it) })
    println(result)

    val resultT = sequenceT(list.map { Option(it) })
    println(resultT)
}

fun <A, B> lift(f: (A) -> B): (Option<A>) -> Option<B> = { it.map(f) }

fun <A, B> cLift(f: (A) -> B): (Option<A>) -> Option<B> = {
    try {
        it.map(f)
    } catch (_: Exception) {
        Option()
    }
}

fun <A, B> hLift(f: (A) -> B): (A) -> Option<B> = {
    try {
        Option(it).map(f)
    } catch (_: Exception) {
        Option()
    }
}

fun <A, B, C> map2(
    oa: Option<A>,
    ob: Option<B>,
    f: (A) -> (B) -> C
): Option<C> =
    oa.flatMap { a -> ob.map { b -> f(a)(b) } }

fun <A, B, C, D> map3(
    oa: Option<A>,
    ob: Option<B>,
    oc: Option<C>,
    f: (A) -> (B) -> (C) -> D
): Option<D> =
    oa.flatMap { a -> ob.flatMap { b -> oc.map { c -> f(a)(b)(c) } } }

sealed class List<out A> {
    abstract fun isEmpty(): Boolean

    fun <B> foldRight(identity: B, f: (A) -> (B) -> B): B = foldRight(this, identity, f)

    fun cons(a: @UnsafeVariance A): List<A> = Cons(a, this)

    fun <B> map(f: (A) -> B): List<B> = foldLeft(Nil) { acc: List<B> -> { h: A -> Cons(f(h), acc) } }.reverse()

    fun reverse(): List<A> = foldLeft(Nil as List<A>) { acc -> { acc.cons(it) } }

    fun <B> foldLeft(identity: B, f: (B) -> (A) -> B): B = foldLeft(identity, this, f)

    internal object Nil : List<Nothing>() {
        override fun isEmpty() = true
        override fun toString(): String = "[NIL]"
    }

    internal class Cons<A>(
        internal val head: A,
        internal val tail: List<A>
    ) : List<A>() {
        override fun isEmpty() = false
        override fun toString(): String = "[${toString("", this)}NIL]"
        private tailrec fun toString(acc: String, list: List<A>): String =
            when (list) {
                is Nil -> acc
                is Cons -> toString("$acc${list.head}, ", list.tail)
            }
    }

    companion object {
        operator
        fun <A> invoke(vararg az: A): List<A> =
            az.foldRight(Nil as List<A>) { a: A, list: List<A> ->
                Cons(a, list)
            }

        private fun <A, B> foldRight(list: List<A>, identity: B, f: (A) -> (B) -> B): B =
            when (list) {
                Nil -> identity
                is Cons -> f(list.head)(foldRight(list.tail, identity, f))
            }

        tailrec fun <A, B> foldLeft(acc: B, list: List<A>, f: (B) -> (A) -> B): B =
            when(list) {
                Nil -> acc
                is Cons -> foldLeft(f(acc)(list.head), list.tail, f)
            }
    }
}

fun <A> sequence(list: List<Option<A>>): Option<List<A>> =
    list.foldRight(Option(List())) { x ->
        { y: Option<List<A>> -> map2(x, y) { a ->
            { b: List<A> -> b.cons(a) } }
        }
    }

fun <A, B> traverse(list: List<A> , f: (A) -> Option<B>): Option<List<B>> =
    list.foldRight(Option(List())) { x ->
        { y: Option<List<B>> ->
            map2(f(x), y) { a ->
                { b: List<B> ->
                    b.cons(a)
                }
            }
        }
    }

fun <A> sequenceT(list: List<Option<A>>): Option<List<A>> =
    traverse(list) { x -> x }
