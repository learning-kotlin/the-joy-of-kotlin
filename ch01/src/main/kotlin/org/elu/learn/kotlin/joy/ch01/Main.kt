package org.elu.learn.kotlin.joy.ch01

fun main() {
    val creditCard = CreditCard()
    val donut = buyDonutWithSideEffect(creditCard)
    println("Donut: $donut")

    val purchase = buyDonut(creditCard)
    println(purchase)

    val purchase1 = buyDonuts(5, creditCard)
    println(purchase1)

    val purchase2 = buyDonuts(creditCard = creditCard)
    println(purchase2)
}

fun buyDonuts(quantity: Int = 1, creditCard: CreditCard): PurchaseList =
    PurchaseList(List(quantity) {
        Donut()
    }, Payment(creditCard, Donut.price * quantity))

fun buyDonut(creditCard: CreditCard) = Purchase(Donut(), Payment(creditCard, Donut.price))

fun buyDonutWithSideEffect(creditCard: CreditCard): Donut {
    val donut = Donut()
    // the line below is a side effect
    creditCard.charge(Donut.price)
    return donut
}
