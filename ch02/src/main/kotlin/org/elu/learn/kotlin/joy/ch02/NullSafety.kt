package org.elu.learn.kotlin.joy.ch02

import kotlin.random.Random

fun nullSafetyExample() {
    val x: Int = 3
    val y: Int? = x
    // next line won't compile
    // val z: Int = y

    val s: String? = someFunctionReturningAStringThatCanBeNull()
    // next won't compile
    //val l = s.length
    val l = if (s != null) s.length else null
    println(l)
    val ll = s?.length
    println(ll)

    // Elvis operator
    val lll = s?.length ?: 0
    println(lll)
}

fun someFunctionReturningAStringThatCanBeNull(): String? =
    if (Random.nextBoolean()) "string" else null
