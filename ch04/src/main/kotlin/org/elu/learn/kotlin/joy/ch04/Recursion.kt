package org.elu.learn.kotlin.joy.ch04

fun runRecursion() {
    // implementing corecursion
    implementCoresursion()
    println()

    // implementing recursion
    implementRecursion()
    println()

    // tail call elimination
    tailCallElimination()
    println()

    // switching from loops to corecursion
    switchToCoRecursion()
    println()

    // exercise 4.1
    exercise1()
    // exercise 4.2
    exercise2()
}

val list = listOf('a', 'b', 'c')

fun append(s: String, c: Char): String = "$s$c"

fun implementCoresursion() {
    fun toString(list: List<Char>, s: String): String =
        if (list.isEmpty()) {
            s
        } else {
            toString(list.subList(1, list.size), append(s, list[0]))
        }
    println(toString(list, ""))

    // more idiomatic solution
    fun toString1(list: List<Char>, s: String): String =
        if (list.isEmpty()) {
            s
        } else {
            toString1(list.drop(1), append(s, list.first()))
        }
    println(toString1(list, ""))

    // using inner function to reduce number of parameters
    fun toString2(list: List<Char>): String {
        fun toString(list: List<Char>, s: String): String =
            if (list.isEmpty()) {
                s
            } else {
                toString(list.subList(1, list.size), append(s, list[0]))
            }
        return toString(list, "")
    }
    println(toString2(list))

    // using default parameters
    fun toString3(list: List<Char>, s: String = ""): String =
        if (list.isEmpty()) {
            s
        } else {
            toString3(list.subList(1, list.size), append(s, list[0]))
        }
    println(toString3(list))
}

fun prepend(c: Char, s: String): String = "$c$s"

fun implementRecursion() {
    fun toString(list: List<Char>): String {
        fun toString(list: List<Char>, s: String): String =
            if (list.isEmpty()) {
                s
            } else {
                toString(list.subList(0, list.size - 1), prepend(list[list.size - 1], s))
            }
        return toString(list, "")
    }
    println(toString(list))

    // recursive implementation
    fun toString1(list: List<Char>): String =
        if (list.isEmpty()) {
            ""
        } else {
            prepend(list[0], toString1(list.subList(1, list.size)))
        }
    println(toString1(list))
}

fun tailCallElimination() {
    // recursive call
    fun toString(list: List<Char>): String {
        fun toString(list: List<Char>, s: String): String =
            if (list.isEmpty()) {
                s
            } else {
                toString(list.subList(1, list.size), append(s, list[0]))
            }
        return toString(list, "")
    }
    println(toString(list))

    // imperative loop
    fun toStringCorec2(list: List<Char>): String {
        var s = ""
        for (c in list) s = append(s, c)
        return s
    }
    println(toStringCorec2(list))

    // using tail call elimination
    fun toString2(list: List<Char>): String {
        tailrec fun toString(list: List<Char>, s: String): String =
            if (list.isEmpty()) {
                s
            } else {
                toString(list.subList(1, list.size), append(s, list[0]))
            }
        return toString(list, "")
    }
    println(toString2(list))
}

fun switchToCoRecursion() {
    // recursive example
    fun sum(n: Int): Int = if (n < 1) 0 else n + sum(n - 1)
    println(sum(5)) // will fail with big number

    // imperative loop solution
    fun sum1(n: Int): Int {
        var sum = 0
        var idx = 0
        while (idx <= n) {
            sum += idx
            idx += 1
        }
        return sum
    }
    println(sum1(5))

    // helper function
    fun sumHelper(n: Int, sum: Int, idx: Int): Int =
        if (idx < 1) sum else sumHelper(n, sum + idx, idx -1)
    fun sum2(n: Int) = sumHelper(n, 0, n)
    println(sum2(5))

    // same as above using inner function, this is recursion
    fun sum3(n: Int): Int {
        fun sum(sum: Int, idx: Int): Int =
            if (idx < 1) sum else sum(sum + idx, idx - 1)
        return sum(0, n)
    }
    println(sum3(5))

    // tail recursion
    fun sum4(n: Int): Int {
        tailrec fun sum(s: Int, i: Int): Int = if (i > n) s else sum(s + i, i + 1)
        return sum(0, 0)
    }
    println(sum4(5))
}

fun inc(n: Int) = n + 1
fun dec(n: Int) = n - 1

fun exercise1() {
    fun add(a: Int, b: Int): Int {
        tailrec fun add(x: Int, y: Int): Int = if (y == 0) x else add(inc(x), dec(y))
        return add(a, b)
    }
    println(add(2, 3))
}

fun factorial(n: Int): Int = if (n == 0) 1 else n * factorial(n - 1)

fun exercise2() {
    println(factorial(5))
    // next value function will not compile
//    val factorialVal: (Int) -> Int = { x ->
//        if (x <= 1) x else x * factorialVal(x - 1)
//    }
    println(Factorial.factorial(5))
    println(Factorial1.factorial(5))
}

// solution with lateinit
object Factorial {
    lateinit var factorial: (Int) -> Int
    init {
        factorial = { x -> if (x <= 1) x else x * factorial(x - 1)}
    }
}
// solution with lazy initialization
object Factorial1 {
    val factorial: (Int) -> Int by lazy {{ x: Int ->
        if (x <= 1) x else x * factorial(x - 1)
    }}
}
